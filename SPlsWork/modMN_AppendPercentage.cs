using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_MODMN_APPENDPERCENTAGE
{
    public class UserModuleClass_MODMN_APPENDPERCENTAGE : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        Crestron.Logos.SplusObjects.AnalogInput ANALOG_IN;
        Crestron.Logos.SplusObjects.StringOutput ANALOG_PERCENTAGE;
        object ANALOG_IN_OnChange_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                
                __context__.SourceCodeLine = 179;
                MakeString ( ANALOG_PERCENTAGE , "{0:d}%", (short)ANALOG_IN  .UshortValue) ; 
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    
    public override void LogosSplusInitialize()
    {
        _SplusNVRAM = new SplusNVRAM( this );
        
        ANALOG_IN = new Crestron.Logos.SplusObjects.AnalogInput( ANALOG_IN__AnalogSerialInput__, this );
        m_AnalogInputList.Add( ANALOG_IN__AnalogSerialInput__, ANALOG_IN );
        
        ANALOG_PERCENTAGE = new Crestron.Logos.SplusObjects.StringOutput( ANALOG_PERCENTAGE__AnalogSerialOutput__, this );
        m_StringOutputList.Add( ANALOG_PERCENTAGE__AnalogSerialOutput__, ANALOG_PERCENTAGE );
        
        
        ANALOG_IN.OnAnalogChange.Add( new InputChangeHandlerWrapper( ANALOG_IN_OnChange_0, false ) );
        
        _SplusNVRAM.PopulateCustomAttributeList( true );
        
        NVRAM = _SplusNVRAM;
        
    }
    
    public override void LogosSimplSharpInitialize()
    {
        
        
    }
    
    public UserModuleClass_MODMN_APPENDPERCENTAGE ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}
    
    
    
    
    const uint ANALOG_IN__AnalogSerialInput__ = 0;
    const uint ANALOG_PERCENTAGE__AnalogSerialOutput__ = 0;
    
    [SplusStructAttribute(-1, true, false)]
    public class SplusNVRAM : SplusStructureBase
    {
    
        public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
        
        
    }
    
    SplusNVRAM _SplusNVRAM = null;
    
    public class __CEvent__ : CEvent
    {
        public __CEvent__() {}
        public void Close() { base.Close(); }
        public int Reset() { return base.Reset() ? 1 : 0; }
        public int Set() { return base.Set() ? 1 : 0; }
        public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
    }
    public class __CMutex__ : CMutex
    {
        public __CMutex__() {}
        public void Close() { base.Close(); }
        public void ReleaseMutex() { base.ReleaseMutex(); }
        public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
    }
     public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
