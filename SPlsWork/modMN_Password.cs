using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_MODMN_PASSWORD
{
    public class UserModuleClass_MODMN_PASSWORD : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        Crestron.Logos.SplusObjects.DigitalInput INCREMENT;
        Crestron.Logos.SplusObjects.DigitalInput DECREMENT;
        Crestron.Logos.SplusObjects.DigitalInput CLEAR;
        Crestron.Logos.SplusObjects.DigitalInput DIGIT1_SEL;
        Crestron.Logos.SplusObjects.DigitalInput DIGIT2_SEL;
        Crestron.Logos.SplusObjects.DigitalInput DIGIT3_SEL;
        Crestron.Logos.SplusObjects.DigitalInput DIGIT4_SEL;
        Crestron.Logos.SplusObjects.DigitalOutput PW_CORRECT;
        Crestron.Logos.SplusObjects.AnalogOutput DIGIT1_FB;
        Crestron.Logos.SplusObjects.AnalogOutput DIGIT2_FB;
        Crestron.Logos.SplusObjects.AnalogOutput DIGIT3_FB;
        Crestron.Logos.SplusObjects.AnalogOutput DIGIT4_FB;
        UShortParameter PASSWORD;
        ushort THOUSANDS = 0;
        ushort HUNDREDS = 0;
        ushort TENS = 0;
        ushort UNITS = 0;
        private ushort INC (  SplusExecutionContext __context__, ushort NUM ) 
            { 
            
            __context__.SourceCodeLine = 144;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( NUM < 9 ))  ) ) 
                { 
                __context__.SourceCodeLine = 145;
                NUM = (ushort) ( (NUM + 1) ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 147;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (NUM == 9))  ) ) 
                    { 
                    __context__.SourceCodeLine = 148;
                    NUM = (ushort) ( 0 ) ; 
                    } 
                
                }
            
            __context__.SourceCodeLine = 150;
            return (ushort)( NUM) ; 
            
            }
            
        private ushort DEC (  SplusExecutionContext __context__, ushort NUM ) 
            { 
            
            __context__.SourceCodeLine = 155;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( NUM > 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 156;
                NUM = (ushort) ( (NUM - 1) ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 158;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (NUM == 0))  ) ) 
                    { 
                    __context__.SourceCodeLine = 159;
                    NUM = (ushort) ( 9 ) ; 
                    } 
                
                }
            
            __context__.SourceCodeLine = 161;
            return (ushort)( NUM) ; 
            
            }
            
        private void CHECKPW (  SplusExecutionContext __context__ ) 
            { 
            
            __context__.SourceCodeLine = 167;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (((((THOUSANDS * 1000) + (HUNDREDS * 100)) + (TENS * 10)) + UNITS) == PASSWORD  .Value))  ) ) 
                { 
                __context__.SourceCodeLine = 168;
                Functions.Pulse ( 25, PW_CORRECT ) ; 
                } 
            
            
            }
            
        object INCREMENT_OnPush_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                
                __context__.SourceCodeLine = 203;
                if ( Functions.TestForTrue  ( ( DIGIT1_SEL  .Value)  ) ) 
                    { 
                    __context__.SourceCodeLine = 205;
                    THOUSANDS = (ushort) ( INC( __context__ , (ushort)( THOUSANDS ) ) ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 208;
                    if ( Functions.TestForTrue  ( ( DIGIT2_SEL  .Value)  ) ) 
                        { 
                        __context__.SourceCodeLine = 210;
                        HUNDREDS = (ushort) ( INC( __context__ , (ushort)( HUNDREDS ) ) ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 213;
                        if ( Functions.TestForTrue  ( ( DIGIT3_SEL  .Value)  ) ) 
                            { 
                            __context__.SourceCodeLine = 215;
                            TENS = (ushort) ( INC( __context__ , (ushort)( TENS ) ) ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 218;
                            if ( Functions.TestForTrue  ( ( DIGIT4_SEL  .Value)  ) ) 
                                { 
                                __context__.SourceCodeLine = 220;
                                UNITS = (ushort) ( INC( __context__ , (ushort)( UNITS ) ) ) ; 
                                } 
                            
                            }
                        
                        }
                    
                    }
                
                __context__.SourceCodeLine = 224;
                DIGIT1_FB  .Value = (ushort) ( THOUSANDS ) ; 
                __context__.SourceCodeLine = 225;
                DIGIT2_FB  .Value = (ushort) ( HUNDREDS ) ; 
                __context__.SourceCodeLine = 226;
                DIGIT3_FB  .Value = (ushort) ( TENS ) ; 
                __context__.SourceCodeLine = 227;
                DIGIT4_FB  .Value = (ushort) ( UNITS ) ; 
                __context__.SourceCodeLine = 229;
                CHECKPW (  __context__  ) ; 
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    object DECREMENT_OnPush_1 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            
            __context__.SourceCodeLine = 234;
            if ( Functions.TestForTrue  ( ( DIGIT1_SEL  .Value)  ) ) 
                { 
                __context__.SourceCodeLine = 236;
                THOUSANDS = (ushort) ( DEC( __context__ , (ushort)( THOUSANDS ) ) ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 239;
                if ( Functions.TestForTrue  ( ( DIGIT2_SEL  .Value)  ) ) 
                    { 
                    __context__.SourceCodeLine = 241;
                    HUNDREDS = (ushort) ( DEC( __context__ , (ushort)( HUNDREDS ) ) ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 244;
                    if ( Functions.TestForTrue  ( ( DIGIT3_SEL  .Value)  ) ) 
                        { 
                        __context__.SourceCodeLine = 246;
                        TENS = (ushort) ( DEC( __context__ , (ushort)( TENS ) ) ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 249;
                        if ( Functions.TestForTrue  ( ( DIGIT4_SEL  .Value)  ) ) 
                            { 
                            __context__.SourceCodeLine = 251;
                            UNITS = (ushort) ( DEC( __context__ , (ushort)( UNITS ) ) ) ; 
                            } 
                        
                        }
                    
                    }
                
                }
            
            __context__.SourceCodeLine = 256;
            DIGIT1_FB  .Value = (ushort) ( THOUSANDS ) ; 
            __context__.SourceCodeLine = 257;
            DIGIT2_FB  .Value = (ushort) ( HUNDREDS ) ; 
            __context__.SourceCodeLine = 258;
            DIGIT3_FB  .Value = (ushort) ( TENS ) ; 
            __context__.SourceCodeLine = 259;
            DIGIT4_FB  .Value = (ushort) ( UNITS ) ; 
            __context__.SourceCodeLine = 261;
            CHECKPW (  __context__  ) ; 
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
object CLEAR_OnPush_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 267;
        THOUSANDS = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 268;
        HUNDREDS = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 269;
        TENS = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 270;
        UNITS = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 272;
        DIGIT1_FB  .Value = (ushort) ( THOUSANDS ) ; 
        __context__.SourceCodeLine = 273;
        DIGIT2_FB  .Value = (ushort) ( HUNDREDS ) ; 
        __context__.SourceCodeLine = 274;
        DIGIT3_FB  .Value = (ushort) ( TENS ) ; 
        __context__.SourceCodeLine = 275;
        DIGIT4_FB  .Value = (ushort) ( UNITS ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}


public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    
    INCREMENT = new Crestron.Logos.SplusObjects.DigitalInput( INCREMENT__DigitalInput__, this );
    m_DigitalInputList.Add( INCREMENT__DigitalInput__, INCREMENT );
    
    DECREMENT = new Crestron.Logos.SplusObjects.DigitalInput( DECREMENT__DigitalInput__, this );
    m_DigitalInputList.Add( DECREMENT__DigitalInput__, DECREMENT );
    
    CLEAR = new Crestron.Logos.SplusObjects.DigitalInput( CLEAR__DigitalInput__, this );
    m_DigitalInputList.Add( CLEAR__DigitalInput__, CLEAR );
    
    DIGIT1_SEL = new Crestron.Logos.SplusObjects.DigitalInput( DIGIT1_SEL__DigitalInput__, this );
    m_DigitalInputList.Add( DIGIT1_SEL__DigitalInput__, DIGIT1_SEL );
    
    DIGIT2_SEL = new Crestron.Logos.SplusObjects.DigitalInput( DIGIT2_SEL__DigitalInput__, this );
    m_DigitalInputList.Add( DIGIT2_SEL__DigitalInput__, DIGIT2_SEL );
    
    DIGIT3_SEL = new Crestron.Logos.SplusObjects.DigitalInput( DIGIT3_SEL__DigitalInput__, this );
    m_DigitalInputList.Add( DIGIT3_SEL__DigitalInput__, DIGIT3_SEL );
    
    DIGIT4_SEL = new Crestron.Logos.SplusObjects.DigitalInput( DIGIT4_SEL__DigitalInput__, this );
    m_DigitalInputList.Add( DIGIT4_SEL__DigitalInput__, DIGIT4_SEL );
    
    PW_CORRECT = new Crestron.Logos.SplusObjects.DigitalOutput( PW_CORRECT__DigitalOutput__, this );
    m_DigitalOutputList.Add( PW_CORRECT__DigitalOutput__, PW_CORRECT );
    
    DIGIT1_FB = new Crestron.Logos.SplusObjects.AnalogOutput( DIGIT1_FB__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( DIGIT1_FB__AnalogSerialOutput__, DIGIT1_FB );
    
    DIGIT2_FB = new Crestron.Logos.SplusObjects.AnalogOutput( DIGIT2_FB__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( DIGIT2_FB__AnalogSerialOutput__, DIGIT2_FB );
    
    DIGIT3_FB = new Crestron.Logos.SplusObjects.AnalogOutput( DIGIT3_FB__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( DIGIT3_FB__AnalogSerialOutput__, DIGIT3_FB );
    
    DIGIT4_FB = new Crestron.Logos.SplusObjects.AnalogOutput( DIGIT4_FB__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( DIGIT4_FB__AnalogSerialOutput__, DIGIT4_FB );
    
    PASSWORD = new UShortParameter( PASSWORD__Parameter__, this );
    m_ParameterList.Add( PASSWORD__Parameter__, PASSWORD );
    
    
    INCREMENT.OnDigitalPush.Add( new InputChangeHandlerWrapper( INCREMENT_OnPush_0, false ) );
    DECREMENT.OnDigitalPush.Add( new InputChangeHandlerWrapper( DECREMENT_OnPush_1, false ) );
    CLEAR.OnDigitalPush.Add( new InputChangeHandlerWrapper( CLEAR_OnPush_2, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_MODMN_PASSWORD ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}




const uint INCREMENT__DigitalInput__ = 0;
const uint DECREMENT__DigitalInput__ = 1;
const uint CLEAR__DigitalInput__ = 2;
const uint DIGIT1_SEL__DigitalInput__ = 3;
const uint DIGIT2_SEL__DigitalInput__ = 4;
const uint DIGIT3_SEL__DigitalInput__ = 5;
const uint DIGIT4_SEL__DigitalInput__ = 6;
const uint PW_CORRECT__DigitalOutput__ = 0;
const uint DIGIT1_FB__AnalogSerialOutput__ = 0;
const uint DIGIT2_FB__AnalogSerialOutput__ = 1;
const uint DIGIT3_FB__AnalogSerialOutput__ = 2;
const uint DIGIT4_FB__AnalogSerialOutput__ = 3;
const uint PASSWORD__Parameter__ = 10;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
