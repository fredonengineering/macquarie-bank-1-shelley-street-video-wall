using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_MODMN_VOLUMEFB
{
    public class UserModuleClass_MODMN_VOLUMEFB : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        Crestron.Logos.SplusObjects.DigitalInput AUDIO_MUTE_FB;
        Crestron.Logos.SplusObjects.DigitalInput VIDEO_MUTE_FB;
        Crestron.Logos.SplusObjects.AnalogInput VOLUME_FB;
        Crestron.Logos.SplusObjects.StringOutput FORMATTED_FB;
        private void OUTPUT (  SplusExecutionContext __context__ ) 
            { 
            
            __context__.SourceCodeLine = 130;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( AUDIO_MUTE_FB  .Value ) && Functions.TestForTrue ( VIDEO_MUTE_FB  .Value )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 132;
                MakeString ( FORMATTED_FB , "Volume: {0:d}% [AV MUTED]", (short)VOLUME_FB  .UshortValue) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 134;
                if ( Functions.TestForTrue  ( ( AUDIO_MUTE_FB  .Value)  ) ) 
                    { 
                    __context__.SourceCodeLine = 136;
                    MakeString ( FORMATTED_FB , "Volume: {0:d}% [AUDIO MUTED]", (short)VOLUME_FB  .UshortValue) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 138;
                    if ( Functions.TestForTrue  ( ( VIDEO_MUTE_FB  .Value)  ) ) 
                        { 
                        __context__.SourceCodeLine = 140;
                        MakeString ( FORMATTED_FB , "Volume: {0:d}% [VIDEO MUTED]", (short)VOLUME_FB  .UshortValue) ; 
                        } 
                    
                    else 
                        { 
                        __context__.SourceCodeLine = 144;
                        MakeString ( FORMATTED_FB , "Volume: {0:d}%", (short)VOLUME_FB  .UshortValue) ; 
                        } 
                    
                    }
                
                }
            
            
            }
            
        object AUDIO_MUTE_FB_OnPush_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                
                __context__.SourceCodeLine = 180;
                OUTPUT (  __context__  ) ; 
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    object AUDIO_MUTE_FB_OnRelease_1 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            
            __context__.SourceCodeLine = 188;
            OUTPUT (  __context__  ) ; 
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
object VOLUME_FB_OnChange_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 196;
        OUTPUT (  __context__  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}


public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    
    AUDIO_MUTE_FB = new Crestron.Logos.SplusObjects.DigitalInput( AUDIO_MUTE_FB__DigitalInput__, this );
    m_DigitalInputList.Add( AUDIO_MUTE_FB__DigitalInput__, AUDIO_MUTE_FB );
    
    VIDEO_MUTE_FB = new Crestron.Logos.SplusObjects.DigitalInput( VIDEO_MUTE_FB__DigitalInput__, this );
    m_DigitalInputList.Add( VIDEO_MUTE_FB__DigitalInput__, VIDEO_MUTE_FB );
    
    VOLUME_FB = new Crestron.Logos.SplusObjects.AnalogInput( VOLUME_FB__AnalogSerialInput__, this );
    m_AnalogInputList.Add( VOLUME_FB__AnalogSerialInput__, VOLUME_FB );
    
    FORMATTED_FB = new Crestron.Logos.SplusObjects.StringOutput( FORMATTED_FB__AnalogSerialOutput__, this );
    m_StringOutputList.Add( FORMATTED_FB__AnalogSerialOutput__, FORMATTED_FB );
    
    
    AUDIO_MUTE_FB.OnDigitalPush.Add( new InputChangeHandlerWrapper( AUDIO_MUTE_FB_OnPush_0, false ) );
    VIDEO_MUTE_FB.OnDigitalPush.Add( new InputChangeHandlerWrapper( AUDIO_MUTE_FB_OnPush_0, false ) );
    AUDIO_MUTE_FB.OnDigitalRelease.Add( new InputChangeHandlerWrapper( AUDIO_MUTE_FB_OnRelease_1, false ) );
    VIDEO_MUTE_FB.OnDigitalRelease.Add( new InputChangeHandlerWrapper( AUDIO_MUTE_FB_OnRelease_1, false ) );
    VOLUME_FB.OnAnalogChange.Add( new InputChangeHandlerWrapper( VOLUME_FB_OnChange_2, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_MODMN_VOLUMEFB ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}




const uint AUDIO_MUTE_FB__DigitalInput__ = 0;
const uint VIDEO_MUTE_FB__DigitalInput__ = 1;
const uint VOLUME_FB__AnalogSerialInput__ = 0;
const uint FORMATTED_FB__AnalogSerialOutput__ = 0;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
