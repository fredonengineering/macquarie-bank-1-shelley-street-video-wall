using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_POMT_DUAL_ANALOG_TO_SERIAL
{
    public class UserModuleClass_POMT_DUAL_ANALOG_TO_SERIAL : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        Crestron.Logos.SplusObjects.AnalogInput IN1;
        Crestron.Logos.SplusObjects.AnalogInput IN2;
        Crestron.Logos.SplusObjects.AnalogOutput OUT;
        object IN1_OnChange_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                
                __context__.SourceCodeLine = 181;
                OUT  .Value = (ushort) ( ((IN1  .UshortValue << 8) + IN2  .UshortValue) ) ; 
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    
    public override void LogosSplusInitialize()
    {
        _SplusNVRAM = new SplusNVRAM( this );
        
        IN1 = new Crestron.Logos.SplusObjects.AnalogInput( IN1__AnalogSerialInput__, this );
        m_AnalogInputList.Add( IN1__AnalogSerialInput__, IN1 );
        
        IN2 = new Crestron.Logos.SplusObjects.AnalogInput( IN2__AnalogSerialInput__, this );
        m_AnalogInputList.Add( IN2__AnalogSerialInput__, IN2 );
        
        OUT = new Crestron.Logos.SplusObjects.AnalogOutput( OUT__AnalogSerialOutput__, this );
        m_AnalogOutputList.Add( OUT__AnalogSerialOutput__, OUT );
        
        
        IN1.OnAnalogChange.Add( new InputChangeHandlerWrapper( IN1_OnChange_0, false ) );
        IN2.OnAnalogChange.Add( new InputChangeHandlerWrapper( IN1_OnChange_0, false ) );
        
        _SplusNVRAM.PopulateCustomAttributeList( true );
        
        NVRAM = _SplusNVRAM;
        
    }
    
    public override void LogosSimplSharpInitialize()
    {
        
        
    }
    
    public UserModuleClass_POMT_DUAL_ANALOG_TO_SERIAL ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}
    
    
    
    
    const uint IN1__AnalogSerialInput__ = 0;
    const uint IN2__AnalogSerialInput__ = 1;
    const uint OUT__AnalogSerialOutput__ = 0;
    
    [SplusStructAttribute(-1, true, false)]
    public class SplusNVRAM : SplusStructureBase
    {
    
        public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
        
        
    }
    
    SplusNVRAM _SplusNVRAM = null;
    
    public class __CEvent__ : CEvent
    {
        public __CEvent__() {}
        public void Close() { base.Close(); }
        public int Reset() { return base.Reset() ? 1 : 0; }
        public int Set() { return base.Set() ? 1 : 0; }
        public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
    }
    public class __CMutex__ : CMutex
    {
        public __CMutex__() {}
        public void Close() { base.Close(); }
        public void ReleaseMutex() { base.ReleaseMutex(); }
        public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
    }
     public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
