using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_MODMN_SCHEDULINGTIMER
{
    public class UserModuleClass_MODMN_SCHEDULINGTIMER : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        Crestron.Logos.SplusObjects.DigitalInput ENABLEMASTER;
        Crestron.Logos.SplusObjects.DigitalInput ENABLEMON;
        Crestron.Logos.SplusObjects.DigitalInput ENABLETUE;
        Crestron.Logos.SplusObjects.DigitalInput ENABLEWED;
        Crestron.Logos.SplusObjects.DigitalInput ENABLETHU;
        Crestron.Logos.SplusObjects.DigitalInput ENABLEFRI;
        Crestron.Logos.SplusObjects.DigitalInput ENABLESAT;
        Crestron.Logos.SplusObjects.DigitalInput ENABLESUN;
        Crestron.Logos.SplusObjects.DigitalOutput SYSTEMON;
        Crestron.Logos.SplusObjects.DigitalOutput SYSTEMOFF;
        UShortParameter MONON_HR;
        UShortParameter MONON_MIN;
        UShortParameter MONOFF_HR;
        UShortParameter MONOFF_MIN;
        UShortParameter TUEON_HR;
        UShortParameter TUEON_MIN;
        UShortParameter TUEOFF_HR;
        UShortParameter TUEOFF_MIN;
        UShortParameter WEDON_HR;
        UShortParameter WEDON_MIN;
        UShortParameter WEDOFF_HR;
        UShortParameter WEDOFF_MIN;
        UShortParameter THUON_HR;
        UShortParameter THUON_MIN;
        UShortParameter THUOFF_HR;
        UShortParameter THUOFF_MIN;
        UShortParameter FRION_HR;
        UShortParameter FRION_MIN;
        UShortParameter FRIOFF_HR;
        UShortParameter FRIOFF_MIN;
        UShortParameter SATON_HR;
        UShortParameter SATON_MIN;
        UShortParameter SATOFF_HR;
        UShortParameter SATOFF_MIN;
        UShortParameter SUNON_HR;
        UShortParameter SUNON_MIN;
        UShortParameter SUNOFF_HR;
        UShortParameter SUNOFF_MIN;
        object ENABLEMASTER_OnPush_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                
                __context__.SourceCodeLine = 198;
                while ( Functions.TestForTrue  ( ( ENABLEMASTER  .Value)  ) ) 
                    { 
                    __context__.SourceCodeLine = 202;
                    
                        {
                        int __SPLS_TMPVAR__SWTCH_1__ = ((int)Functions.GetDayOfWeekNum());
                        
                            { 
                            if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 0) ) ) ) 
                                { 
                                __context__.SourceCodeLine = 206;
                                Print( "Sunday\r\n") ; 
                                __context__.SourceCodeLine = 207;
                                if ( Functions.TestForTrue  ( ( ENABLESUN  .Value)  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 209;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (SUNON_HR  .Value == Functions.GetHourNum()) ) && Functions.TestForTrue ( Functions.BoolToInt (SUNON_MIN  .Value == Functions.GetMinutesNum()) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (SYSTEMON  .Value == 0) )) ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 211;
                                        SYSTEMON  .Value = (ushort) ( 1 ) ; 
                                        __context__.SourceCodeLine = 212;
                                        SYSTEMOFF  .Value = (ushort) ( 0 ) ; 
                                        __context__.SourceCodeLine = 213;
                                        Print( "System On at {0:d}:{1:d}\r\n", (short)Functions.GetHourNum(), (short)Functions.GetMinutesNum()) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 215;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (SUNOFF_HR  .Value == Functions.GetHourNum()) ) && Functions.TestForTrue ( Functions.BoolToInt (SUNOFF_MIN  .Value == Functions.GetMinutesNum()) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (SYSTEMOFF  .Value == 0) )) ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 217;
                                            SYSTEMON  .Value = (ushort) ( 0 ) ; 
                                            __context__.SourceCodeLine = 218;
                                            SYSTEMOFF  .Value = (ushort) ( 1 ) ; 
                                            __context__.SourceCodeLine = 219;
                                            Print( "System Off at {0:d}:{1:d}\r\n", (short)Functions.GetHourNum(), (short)Functions.GetMinutesNum()) ; 
                                            } 
                                        
                                        }
                                    
                                    } 
                                
                                } 
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 1) ) ) ) 
                                { 
                                __context__.SourceCodeLine = 225;
                                Print( "Monday\r\n") ; 
                                __context__.SourceCodeLine = 226;
                                if ( Functions.TestForTrue  ( ( ENABLEMON  .Value)  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 228;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (MONON_HR  .Value == Functions.GetHourNum()) ) && Functions.TestForTrue ( Functions.BoolToInt (MONON_MIN  .Value == Functions.GetMinutesNum()) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (SYSTEMON  .Value == 0) )) ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 230;
                                        SYSTEMON  .Value = (ushort) ( 1 ) ; 
                                        __context__.SourceCodeLine = 231;
                                        SYSTEMOFF  .Value = (ushort) ( 0 ) ; 
                                        __context__.SourceCodeLine = 232;
                                        Print( "System On at {0:d}:{1:d}\r\n", (short)Functions.GetHourNum(), (short)Functions.GetMinutesNum()) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 234;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (MONOFF_HR  .Value == Functions.GetHourNum()) ) && Functions.TestForTrue ( Functions.BoolToInt (MONOFF_MIN  .Value == Functions.GetMinutesNum()) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (SYSTEMOFF  .Value == 0) )) ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 236;
                                            SYSTEMON  .Value = (ushort) ( 0 ) ; 
                                            __context__.SourceCodeLine = 237;
                                            SYSTEMOFF  .Value = (ushort) ( 1 ) ; 
                                            __context__.SourceCodeLine = 238;
                                            Print( "System Off at {0:d}:{1:d}\r\n", (short)Functions.GetHourNum(), (short)Functions.GetMinutesNum()) ; 
                                            } 
                                        
                                        }
                                    
                                    } 
                                
                                } 
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 2) ) ) ) 
                                { 
                                __context__.SourceCodeLine = 245;
                                Print( "Tuesday\r\n") ; 
                                __context__.SourceCodeLine = 246;
                                if ( Functions.TestForTrue  ( ( ENABLETUE  .Value)  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 248;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (TUEON_HR  .Value == Functions.GetHourNum()) ) && Functions.TestForTrue ( Functions.BoolToInt (TUEON_MIN  .Value == Functions.GetMinutesNum()) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (SYSTEMON  .Value == 0) )) ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 250;
                                        SYSTEMON  .Value = (ushort) ( 1 ) ; 
                                        __context__.SourceCodeLine = 251;
                                        SYSTEMOFF  .Value = (ushort) ( 0 ) ; 
                                        __context__.SourceCodeLine = 252;
                                        Print( "System On at {0:d}:{1:d}\r\n", (short)Functions.GetHourNum(), (short)Functions.GetMinutesNum()) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 254;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (TUEOFF_HR  .Value == Functions.GetHourNum()) ) && Functions.TestForTrue ( Functions.BoolToInt (TUEOFF_MIN  .Value == Functions.GetMinutesNum()) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (SYSTEMOFF  .Value == 0) )) ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 256;
                                            SYSTEMON  .Value = (ushort) ( 0 ) ; 
                                            __context__.SourceCodeLine = 257;
                                            SYSTEMOFF  .Value = (ushort) ( 1 ) ; 
                                            __context__.SourceCodeLine = 258;
                                            Print( "System Off at {0:d}:{1:d}\r\n", (short)Functions.GetHourNum(), (short)Functions.GetMinutesNum()) ; 
                                            } 
                                        
                                        }
                                    
                                    } 
                                
                                } 
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 3) ) ) ) 
                                { 
                                __context__.SourceCodeLine = 265;
                                Print( "Wednesday\r\n") ; 
                                __context__.SourceCodeLine = 266;
                                if ( Functions.TestForTrue  ( ( ENABLEWED  .Value)  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 268;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (WEDON_HR  .Value == Functions.GetHourNum()) ) && Functions.TestForTrue ( Functions.BoolToInt (WEDON_MIN  .Value == Functions.GetMinutesNum()) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (SYSTEMON  .Value == 0) )) ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 270;
                                        SYSTEMON  .Value = (ushort) ( 1 ) ; 
                                        __context__.SourceCodeLine = 271;
                                        SYSTEMOFF  .Value = (ushort) ( 0 ) ; 
                                        __context__.SourceCodeLine = 272;
                                        Print( "System On at {0:d}:{1:d}\r\n", (short)Functions.GetHourNum(), (short)Functions.GetMinutesNum()) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 274;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (WEDOFF_HR  .Value == Functions.GetHourNum()) ) && Functions.TestForTrue ( Functions.BoolToInt (WEDOFF_MIN  .Value == Functions.GetMinutesNum()) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (SYSTEMOFF  .Value == 0) )) ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 276;
                                            SYSTEMON  .Value = (ushort) ( 0 ) ; 
                                            __context__.SourceCodeLine = 277;
                                            SYSTEMOFF  .Value = (ushort) ( 1 ) ; 
                                            __context__.SourceCodeLine = 278;
                                            Print( "System Off at {0:d}:{1:d}\r\n", (short)Functions.GetHourNum(), (short)Functions.GetMinutesNum()) ; 
                                            } 
                                        
                                        }
                                    
                                    } 
                                
                                } 
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 4) ) ) ) 
                                { 
                                __context__.SourceCodeLine = 286;
                                Print( "Thursday\r\n") ; 
                                __context__.SourceCodeLine = 287;
                                if ( Functions.TestForTrue  ( ( ENABLETHU  .Value)  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 289;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (THUON_HR  .Value == Functions.GetHourNum()) ) && Functions.TestForTrue ( Functions.BoolToInt (THUON_MIN  .Value == Functions.GetMinutesNum()) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (SYSTEMON  .Value == 0) )) ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 291;
                                        SYSTEMON  .Value = (ushort) ( 1 ) ; 
                                        __context__.SourceCodeLine = 292;
                                        SYSTEMOFF  .Value = (ushort) ( 0 ) ; 
                                        __context__.SourceCodeLine = 293;
                                        Print( "System On at {0:d}:{1:d}\r\n", (short)Functions.GetHourNum(), (short)Functions.GetMinutesNum()) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 295;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (THUOFF_HR  .Value == Functions.GetHourNum()) ) && Functions.TestForTrue ( Functions.BoolToInt (THUOFF_MIN  .Value == Functions.GetMinutesNum()) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (SYSTEMOFF  .Value == 0) )) ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 297;
                                            SYSTEMON  .Value = (ushort) ( 0 ) ; 
                                            __context__.SourceCodeLine = 298;
                                            SYSTEMOFF  .Value = (ushort) ( 1 ) ; 
                                            __context__.SourceCodeLine = 299;
                                            Print( "System Off at {0:d}:{1:d}\r\n", (short)Functions.GetHourNum(), (short)Functions.GetMinutesNum()) ; 
                                            } 
                                        
                                        }
                                    
                                    } 
                                
                                } 
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 5) ) ) ) 
                                { 
                                __context__.SourceCodeLine = 307;
                                Print( "Friday\r\n") ; 
                                __context__.SourceCodeLine = 308;
                                if ( Functions.TestForTrue  ( ( ENABLEFRI  .Value)  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 310;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (FRION_HR  .Value == Functions.GetHourNum()) ) && Functions.TestForTrue ( Functions.BoolToInt (FRION_MIN  .Value == Functions.GetMinutesNum()) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (SYSTEMON  .Value == 0) )) ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 312;
                                        SYSTEMON  .Value = (ushort) ( 1 ) ; 
                                        __context__.SourceCodeLine = 313;
                                        SYSTEMOFF  .Value = (ushort) ( 0 ) ; 
                                        __context__.SourceCodeLine = 314;
                                        Print( "System On at {0:d}:{1:d}\r\n", (short)Functions.GetHourNum(), (short)Functions.GetMinutesNum()) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 316;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (FRIOFF_HR  .Value == Functions.GetHourNum()) ) && Functions.TestForTrue ( Functions.BoolToInt (FRIOFF_MIN  .Value == Functions.GetMinutesNum()) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (SYSTEMOFF  .Value == 0) )) ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 318;
                                            SYSTEMON  .Value = (ushort) ( 0 ) ; 
                                            __context__.SourceCodeLine = 319;
                                            SYSTEMOFF  .Value = (ushort) ( 1 ) ; 
                                            __context__.SourceCodeLine = 320;
                                            Print( "System Off at {0:d}:{1:d}\r\n", (short)Functions.GetHourNum(), (short)Functions.GetMinutesNum()) ; 
                                            } 
                                        
                                        }
                                    
                                    } 
                                
                                } 
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 6) ) ) ) 
                                { 
                                __context__.SourceCodeLine = 328;
                                if ( Functions.TestForTrue  ( ( ENABLESAT  .Value)  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 330;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (SATON_HR  .Value == Functions.GetHourNum()) ) && Functions.TestForTrue ( Functions.BoolToInt (SATON_MIN  .Value == Functions.GetMinutesNum()) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (SYSTEMON  .Value == 0) )) ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 332;
                                        SYSTEMON  .Value = (ushort) ( 1 ) ; 
                                        __context__.SourceCodeLine = 333;
                                        SYSTEMOFF  .Value = (ushort) ( 0 ) ; 
                                        __context__.SourceCodeLine = 334;
                                        Print( "System On at {0:d}:{1:d}\r\n", (short)Functions.GetHourNum(), (short)Functions.GetMinutesNum()) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 336;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (SATOFF_HR  .Value == Functions.GetHourNum()) ) && Functions.TestForTrue ( Functions.BoolToInt (SATOFF_MIN  .Value == Functions.GetMinutesNum()) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt (SYSTEMOFF  .Value == 0) )) ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 338;
                                            SYSTEMON  .Value = (ushort) ( 0 ) ; 
                                            __context__.SourceCodeLine = 339;
                                            SYSTEMOFF  .Value = (ushort) ( 1 ) ; 
                                            __context__.SourceCodeLine = 340;
                                            Print( "System Off at {0:d}:{1:d}\r\n", (short)Functions.GetHourNum(), (short)Functions.GetMinutesNum()) ; 
                                            } 
                                        
                                        }
                                    
                                    } 
                                
                                } 
                            
                            } 
                            
                        }
                        
                    
                    __context__.SourceCodeLine = 346;
                    Functions.Delay (  (int) ( 500 ) ) ; 
                    __context__.SourceCodeLine = 198;
                    } 
                
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    
    public override void LogosSplusInitialize()
    {
        _SplusNVRAM = new SplusNVRAM( this );
        
        ENABLEMASTER = new Crestron.Logos.SplusObjects.DigitalInput( ENABLEMASTER__DigitalInput__, this );
        m_DigitalInputList.Add( ENABLEMASTER__DigitalInput__, ENABLEMASTER );
        
        ENABLEMON = new Crestron.Logos.SplusObjects.DigitalInput( ENABLEMON__DigitalInput__, this );
        m_DigitalInputList.Add( ENABLEMON__DigitalInput__, ENABLEMON );
        
        ENABLETUE = new Crestron.Logos.SplusObjects.DigitalInput( ENABLETUE__DigitalInput__, this );
        m_DigitalInputList.Add( ENABLETUE__DigitalInput__, ENABLETUE );
        
        ENABLEWED = new Crestron.Logos.SplusObjects.DigitalInput( ENABLEWED__DigitalInput__, this );
        m_DigitalInputList.Add( ENABLEWED__DigitalInput__, ENABLEWED );
        
        ENABLETHU = new Crestron.Logos.SplusObjects.DigitalInput( ENABLETHU__DigitalInput__, this );
        m_DigitalInputList.Add( ENABLETHU__DigitalInput__, ENABLETHU );
        
        ENABLEFRI = new Crestron.Logos.SplusObjects.DigitalInput( ENABLEFRI__DigitalInput__, this );
        m_DigitalInputList.Add( ENABLEFRI__DigitalInput__, ENABLEFRI );
        
        ENABLESAT = new Crestron.Logos.SplusObjects.DigitalInput( ENABLESAT__DigitalInput__, this );
        m_DigitalInputList.Add( ENABLESAT__DigitalInput__, ENABLESAT );
        
        ENABLESUN = new Crestron.Logos.SplusObjects.DigitalInput( ENABLESUN__DigitalInput__, this );
        m_DigitalInputList.Add( ENABLESUN__DigitalInput__, ENABLESUN );
        
        SYSTEMON = new Crestron.Logos.SplusObjects.DigitalOutput( SYSTEMON__DigitalOutput__, this );
        m_DigitalOutputList.Add( SYSTEMON__DigitalOutput__, SYSTEMON );
        
        SYSTEMOFF = new Crestron.Logos.SplusObjects.DigitalOutput( SYSTEMOFF__DigitalOutput__, this );
        m_DigitalOutputList.Add( SYSTEMOFF__DigitalOutput__, SYSTEMOFF );
        
        MONON_HR = new UShortParameter( MONON_HR__Parameter__, this );
        m_ParameterList.Add( MONON_HR__Parameter__, MONON_HR );
        
        MONON_MIN = new UShortParameter( MONON_MIN__Parameter__, this );
        m_ParameterList.Add( MONON_MIN__Parameter__, MONON_MIN );
        
        MONOFF_HR = new UShortParameter( MONOFF_HR__Parameter__, this );
        m_ParameterList.Add( MONOFF_HR__Parameter__, MONOFF_HR );
        
        MONOFF_MIN = new UShortParameter( MONOFF_MIN__Parameter__, this );
        m_ParameterList.Add( MONOFF_MIN__Parameter__, MONOFF_MIN );
        
        TUEON_HR = new UShortParameter( TUEON_HR__Parameter__, this );
        m_ParameterList.Add( TUEON_HR__Parameter__, TUEON_HR );
        
        TUEON_MIN = new UShortParameter( TUEON_MIN__Parameter__, this );
        m_ParameterList.Add( TUEON_MIN__Parameter__, TUEON_MIN );
        
        TUEOFF_HR = new UShortParameter( TUEOFF_HR__Parameter__, this );
        m_ParameterList.Add( TUEOFF_HR__Parameter__, TUEOFF_HR );
        
        TUEOFF_MIN = new UShortParameter( TUEOFF_MIN__Parameter__, this );
        m_ParameterList.Add( TUEOFF_MIN__Parameter__, TUEOFF_MIN );
        
        WEDON_HR = new UShortParameter( WEDON_HR__Parameter__, this );
        m_ParameterList.Add( WEDON_HR__Parameter__, WEDON_HR );
        
        WEDON_MIN = new UShortParameter( WEDON_MIN__Parameter__, this );
        m_ParameterList.Add( WEDON_MIN__Parameter__, WEDON_MIN );
        
        WEDOFF_HR = new UShortParameter( WEDOFF_HR__Parameter__, this );
        m_ParameterList.Add( WEDOFF_HR__Parameter__, WEDOFF_HR );
        
        WEDOFF_MIN = new UShortParameter( WEDOFF_MIN__Parameter__, this );
        m_ParameterList.Add( WEDOFF_MIN__Parameter__, WEDOFF_MIN );
        
        THUON_HR = new UShortParameter( THUON_HR__Parameter__, this );
        m_ParameterList.Add( THUON_HR__Parameter__, THUON_HR );
        
        THUON_MIN = new UShortParameter( THUON_MIN__Parameter__, this );
        m_ParameterList.Add( THUON_MIN__Parameter__, THUON_MIN );
        
        THUOFF_HR = new UShortParameter( THUOFF_HR__Parameter__, this );
        m_ParameterList.Add( THUOFF_HR__Parameter__, THUOFF_HR );
        
        THUOFF_MIN = new UShortParameter( THUOFF_MIN__Parameter__, this );
        m_ParameterList.Add( THUOFF_MIN__Parameter__, THUOFF_MIN );
        
        FRION_HR = new UShortParameter( FRION_HR__Parameter__, this );
        m_ParameterList.Add( FRION_HR__Parameter__, FRION_HR );
        
        FRION_MIN = new UShortParameter( FRION_MIN__Parameter__, this );
        m_ParameterList.Add( FRION_MIN__Parameter__, FRION_MIN );
        
        FRIOFF_HR = new UShortParameter( FRIOFF_HR__Parameter__, this );
        m_ParameterList.Add( FRIOFF_HR__Parameter__, FRIOFF_HR );
        
        FRIOFF_MIN = new UShortParameter( FRIOFF_MIN__Parameter__, this );
        m_ParameterList.Add( FRIOFF_MIN__Parameter__, FRIOFF_MIN );
        
        SATON_HR = new UShortParameter( SATON_HR__Parameter__, this );
        m_ParameterList.Add( SATON_HR__Parameter__, SATON_HR );
        
        SATON_MIN = new UShortParameter( SATON_MIN__Parameter__, this );
        m_ParameterList.Add( SATON_MIN__Parameter__, SATON_MIN );
        
        SATOFF_HR = new UShortParameter( SATOFF_HR__Parameter__, this );
        m_ParameterList.Add( SATOFF_HR__Parameter__, SATOFF_HR );
        
        SATOFF_MIN = new UShortParameter( SATOFF_MIN__Parameter__, this );
        m_ParameterList.Add( SATOFF_MIN__Parameter__, SATOFF_MIN );
        
        SUNON_HR = new UShortParameter( SUNON_HR__Parameter__, this );
        m_ParameterList.Add( SUNON_HR__Parameter__, SUNON_HR );
        
        SUNON_MIN = new UShortParameter( SUNON_MIN__Parameter__, this );
        m_ParameterList.Add( SUNON_MIN__Parameter__, SUNON_MIN );
        
        SUNOFF_HR = new UShortParameter( SUNOFF_HR__Parameter__, this );
        m_ParameterList.Add( SUNOFF_HR__Parameter__, SUNOFF_HR );
        
        SUNOFF_MIN = new UShortParameter( SUNOFF_MIN__Parameter__, this );
        m_ParameterList.Add( SUNOFF_MIN__Parameter__, SUNOFF_MIN );
        
        
        ENABLEMASTER.OnDigitalPush.Add( new InputChangeHandlerWrapper( ENABLEMASTER_OnPush_0, false ) );
        
        _SplusNVRAM.PopulateCustomAttributeList( true );
        
        NVRAM = _SplusNVRAM;
        
    }
    
    public override void LogosSimplSharpInitialize()
    {
        
        
    }
    
    public UserModuleClass_MODMN_SCHEDULINGTIMER ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}
    
    
    
    
    const uint ENABLEMASTER__DigitalInput__ = 0;
    const uint ENABLEMON__DigitalInput__ = 1;
    const uint ENABLETUE__DigitalInput__ = 2;
    const uint ENABLEWED__DigitalInput__ = 3;
    const uint ENABLETHU__DigitalInput__ = 4;
    const uint ENABLEFRI__DigitalInput__ = 5;
    const uint ENABLESAT__DigitalInput__ = 6;
    const uint ENABLESUN__DigitalInput__ = 7;
    const uint SYSTEMON__DigitalOutput__ = 0;
    const uint SYSTEMOFF__DigitalOutput__ = 1;
    const uint MONON_HR__Parameter__ = 10;
    const uint MONON_MIN__Parameter__ = 11;
    const uint MONOFF_HR__Parameter__ = 12;
    const uint MONOFF_MIN__Parameter__ = 13;
    const uint TUEON_HR__Parameter__ = 14;
    const uint TUEON_MIN__Parameter__ = 15;
    const uint TUEOFF_HR__Parameter__ = 16;
    const uint TUEOFF_MIN__Parameter__ = 17;
    const uint WEDON_HR__Parameter__ = 18;
    const uint WEDON_MIN__Parameter__ = 19;
    const uint WEDOFF_HR__Parameter__ = 20;
    const uint WEDOFF_MIN__Parameter__ = 21;
    const uint THUON_HR__Parameter__ = 22;
    const uint THUON_MIN__Parameter__ = 23;
    const uint THUOFF_HR__Parameter__ = 24;
    const uint THUOFF_MIN__Parameter__ = 25;
    const uint FRION_HR__Parameter__ = 26;
    const uint FRION_MIN__Parameter__ = 27;
    const uint FRIOFF_HR__Parameter__ = 28;
    const uint FRIOFF_MIN__Parameter__ = 29;
    const uint SATON_HR__Parameter__ = 30;
    const uint SATON_MIN__Parameter__ = 31;
    const uint SATOFF_HR__Parameter__ = 32;
    const uint SATOFF_MIN__Parameter__ = 33;
    const uint SUNON_HR__Parameter__ = 34;
    const uint SUNON_MIN__Parameter__ = 35;
    const uint SUNOFF_HR__Parameter__ = 36;
    const uint SUNOFF_MIN__Parameter__ = 37;
    
    [SplusStructAttribute(-1, true, false)]
    public class SplusNVRAM : SplusStructureBase
    {
    
        public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
        
        
    }
    
    SplusNVRAM _SplusNVRAM = null;
    
    public class __CEvent__ : CEvent
    {
        public __CEvent__() {}
        public void Close() { base.Close(); }
        public int Reset() { return base.Reset() ? 1 : 0; }
        public int Set() { return base.Set() ? 1 : 0; }
        public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
    }
    public class __CMutex__ : CMutex
    {
        public __CMutex__() {}
        public void Close() { base.Close(); }
        public void ReleaseMutex() { base.ReleaseMutex(); }
        public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
    }
     public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
